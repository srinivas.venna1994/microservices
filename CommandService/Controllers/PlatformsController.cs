using Microsoft.AspNetCore.Mvc;

namespace CommandsService.Controllers{
    [Route("api/c/[controller]")]
    [ApiController]
    public class PlatformsController:ControllerBase{
        public PlatformsController()
        {
            
        }

        [HttpPost]
        public ActionResult TestInboundCnnection(){
            Console.WriteLine("Inbound Post # Command Service");
            return Ok("Inbound Post # Command Service from platform controller");
        }
    }
}