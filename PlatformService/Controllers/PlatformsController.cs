using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PlatformService.Data;
using PlatformService.Dtos;
using PlatformService.Models;
using PlatformService.SyncDataServices.Http;

namespace PlatformService.Controllers{

    [ApiController]
    [Route("api/[controller]")]
    public class PlatformsController:ControllerBase{
        private readonly IPlatformRepo _platform;
        private readonly IMapper _mapper;
        private readonly ICommandDataClient _commandDataClient;

        public PlatformsController(IPlatformRepo platform,IMapper mapper,
        ICommandDataClient commandDataClient)
        {
            _platform = platform;
            _mapper = mapper;
            _commandDataClient =commandDataClient;
        }

        [HttpGet]
        public ActionResult<IEnumerable<PlatformReadDto>> GetPlatforms(){
              Console.WriteLine("Platforms Getting");
              var platformItems = _platform.GetAllPlatforms();
              
              return Ok(_mapper.Map<IEnumerable<PlatformReadDto>>(platformItems));
        }

         [HttpGet("GetPlatform/{id}")]
        public ActionResult<PlatformReadDto> GetPlatformById(int id){
           
              if(_platform.GetAllPlatforms().FirstOrDefault(p=>p.Id == id) != null){
                var platform = _platform.GetAllPlatforms().FirstOrDefault(p=>p.Id == id);
                    return Ok(_mapper.Map<PlatformReadDto>(platform));
              };
              
              return NotFound("The requested platform not found");
        }

             [HttpPost("CreatePlatform")]
        public async Task<ActionResult<PlatformReadDto>> CreatePlatform(PlatformCreateDto platformCreateDto){
            Platform p1 =  _mapper.Map<Platform>(platformCreateDto);
           _platform.CreatePlatform(p1);
           _platform.SaveChanges();
           var platformeadDto = _mapper.Map<PlatformReadDto>(p1);

            try{

               await _commandDataClient.SendPlatformToCommand(platformeadDto);
                 Console.WriteLine($"Could  send sunchrounsly: OK");
           
            }
            catch(Exception ex){

            Console.WriteLine($"Could not send sunchrounsly: {ex.Message}");
            
            }

            return Created(nameof(GetPlatformById),new {Id = platformeadDto.Id,platformeadDto});
          // return CreatedAtRoute( nameof(GetPlatformById),new {Id = platformeadDto.Id,platformeadDto});
              
        }


    }
}