using PlatformService.Models;

namespace PlatformService.Data{

    public class PlatformRepo : IPlatformRepo
    {
        private readonly AppDbContext _context;

        public PlatformRepo(AppDbContext context)
        {
            _context = context;
        }
        public void CreatePlatform(Platform platform)
        {
            if(platform == null){
                throw new ArgumentException(nameof(platform));
            }
            _context.Add(platform);
            _context.SaveChanges();
            
        }

        public IEnumerable<Platform> GetAllPlatforms()
        {
            return _context.Platforms.ToList();
        }

        public Platform GetPlatformById(int id)
        {
            if(_context.Platforms.FirstOrDefault(x=>x.Id == id) != null){
                 
               return   _context.Platforms.FirstOrDefault(x=>x.Id == id);
            }
            
             throw new  ArgumentNullException("Specified platform not available in database");
        }

        public bool SaveChanges()
        {
           return (_context.SaveChanges()>=0);
        }
    }
}